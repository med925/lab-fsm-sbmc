#include "stm32f4xx_hal.h"

#define ACC_CONFIG1_REG 0x20
#define ACC_CONFIG1_RUN_VALUE 0x47

#define ACC_WHO_AM_I_REG 0x0F
#define ACC_WHO_AM_I_VALUE 0x3B

#define ACC_X_REG 0x29
#define ACC_Y_REG 0x2B
#define ACC_Z_REG 0x2D


typedef enum {PAL_IDLE, PAL_CONFIG, PAL_LOGGING} palState_t;

typedef struct{
  palState_t pstate;
  int8_t x,y,z,lx,ly,lz,dx,dy,dz;
}acceleroStruct_t;


void ACC_Init(acceleroStruct_t * acc);
void ACC_Routine(acceleroStruct_t * acc);