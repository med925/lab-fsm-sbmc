//led.h


#include "stm32f4xx.h"

typedef enum {
	LED_ON,
	LED_OFF
}ledState;

typedef enum {
    NO_TRANS=0,
	LED_ON_OFF=0x01,
	LED_OFF_ON=0x02
}ledTransition;

typedef struct{
	ledState state;
	ledTransition trans;
	GPIO_TypeDef* port;
	uint16_t pin;
	uint16_t period;
	uint16_t duty;
	__IO uint16_t timeout;
}ledStruct;
void LED_Init(ledStruct * led,GPIO_TypeDef* port,uint16_t pin,uint16_t period,uint16_t duty);
void LED_Routine(ledStruct * led);
void LED_MsRoutine(ledStruct * led);
