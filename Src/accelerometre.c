#include "accelerometre.h"

extern SPI_HandleTypeDef hspi1;
void ACC_PAL_Init(acceleroStruct_t * acc){
  acc->pstate=PAL_IDLE;
}

void ACC_PAL_WriteReg(uint8_t adr, uint8_t data){
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);
  HAL_SPI_Transmit(&hspi1,&adr,1,1000);
  HAL_SPI_Transmit(&hspi1,&data,1,1000);
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
}
uint8_t ACC_PAL_ReadReg(uint8_t adr){
  uint8_t data=0;
  adr|=0x80;
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);
  HAL_SPI_Transmit(&hspi1,&adr,1,1000);
  HAL_SPI_Receive(&hspi1,&data,1,1000);
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
  return data;
}
void ACC_PAL_Routine(acceleroStruct_t * acc){
  switch (acc->pstate){
  case PAL_IDLE:
    MX_SPI1_Init();
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);//cs_high
    acc->pstate=PAL_CONFIG;
    break;
  case PAL_CONFIG:
    ACC_PAL_WriteReg(ACC_CONFIG1_REG,ACC_CONFIG1_RUN_VALUE);
    acc->pstate=PAL_LOGGING;
    break;
  case PAL_LOGGING:
    if(ACC_PAL_ReadReg(ACC_WHO_AM_I_REG)!=ACC_WHO_AM_I_VALUE){
      acc->pstate=PAL_IDLE;
    }else{
      acc->lx=acc->x;
      acc->ly=acc->y;
      acc->lz=acc->z;
      
      acc->x=ACC_PAL_ReadReg(ACC_X_REG);
      acc->y=ACC_PAL_ReadReg(ACC_Y_REG);
      acc->z=ACC_PAL_ReadReg(ACC_Z_REG);
      
      acc->dx=acc->lx-acc->x;
      acc->dy=acc->ly-acc->y;
      acc->dz=acc->lz-acc->z;
    }
    break;
  }
}


void ACC_Init(acceleroStruct_t * acc){
  ACC_PAL_Init(acc);
  //temp=ACC_PAL_ReadReg(0x0f);
}

void ACC_Routine(acceleroStruct_t * acc){
  ACC_PAL_Routine(acc);
}

