#include "stm32f4xx.h"

#include "led.h"
#include "accelerometre.h"


typedef struct{
  ledStruct leds[4];
  acceleroStruct_t acc;
}MachineStruct;

void MACHINE_init(MachineStruct * machine);
void MACHINE_routines(MachineStruct * machine);