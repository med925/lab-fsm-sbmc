#include "machine.h"

void MACHINE_init(MachineStruct * machine){
  LED_Init(&machine->leds[0],GPIOD,GPIO_PIN_12,1000,500);
  LED_Init(&machine->leds[1],GPIOD,GPIO_PIN_13,1000,500);
  LED_Init(&machine->leds[2],GPIOD,GPIO_PIN_14,1000,500);
  LED_Init(&machine->leds[3],GPIOD,GPIO_PIN_15,1000,500);
  
  ACC_Init(&machine->acc);
}
void MACHINE_routines(MachineStruct * machine){
    LED_Routine(&machine->leds[0]);
    LED_Routine(&machine->leds[1]);
    LED_Routine(&machine->leds[2]);
    LED_Routine(&machine->leds[3]);
    ACC_Routine(&machine->acc);
}

void MACHINE_DecrementDelay(MachineStruct * machine){
  LED_setDuty(&machine->leds[0],500+machine->acc.x*500/64);
  LED_MsRoutine(&machine->leds[0]);
  LED_MsRoutine(&machine->leds[1]);
  LED_MsRoutine(&machine->leds[2]);
  LED_MsRoutine(&machine->leds[3]);
}