#include "led.h"

void LED_Init(ledStruct * led,GPIO_TypeDef* port,uint16_t pin,uint16_t period,uint16_t duty){
	led->port=port;
	led->pin=pin;
	led->period=period;
	led->duty=duty;
	//Low level init
        GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(port, &GPIO_InitStruct);
}
void LED_Routine(ledStruct * led){
	switch(led->state){
	case LED_ON:
		if(led->trans&LED_ON_OFF){
			led->state=LED_OFF;
			led->trans=NO_TRANS;        
     			HAL_GPIO_WritePin(led->port, led->pin,GPIO_PIN_RESET);
		}
	break;
	case LED_OFF:
		if(led->trans&LED_OFF_ON){
			led->state=LED_ON;
			led->trans=NO_TRANS;
             		HAL_GPIO_WritePin(led->port, led->pin,GPIO_PIN_SET);
		}
	break;
	}

}
void LED_MsRoutine(ledStruct * led){
	if(led->timeout){
		led->timeout--;
		if(led->timeout==led->duty)
			led->trans=LED_OFF_ON;
  	}else{
		led->timeout=led->period;
		led->trans=LED_ON_OFF;
  	}
}

void LED_setDuty(ledStruct * led,uint8_t duty){
  led->duty=duty;
}